CREATE DATABASE test;

\c test

CREATE TABLE customer (
    id character varying,
    name character varying,
    language character varying,
    state character varying,
    company character varying,
    gender character varying,
    age integer
);

COPY customer FROM '/backup/customer.csv' CSV;

CREATE TABLE product (
    id character varying,
    name character varying,
    brand character varying,
    brandowner character varying,
    weightgrams double precision,
    weightounce double precision,
    category character varying,
    price double precision,
    cost double precision,
    profit double precision
);

COPY product FROM '/backup/product.csv' CSV;

CREATE TABLE sales (
    order_id character varying,
    cid character varying,
    pid character varying,
    channel_name character varying,
    date date,
    qty bigint,
    total_price double precision
);

COPY sales FROM '/backup/sales.csv' CSV;
