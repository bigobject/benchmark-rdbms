select SUM(qty), customer.gender
from sales
join customer
    on sales.cid = customer.id
group by customer.gender;

select SUM(qty), customer.gender, product.name
from sales
join customer
    on sales.cid = customer.id
join product
    on sales.pid = product.id
group by customer.gender, product.name;

select SUM(qty), customer.gender, product.name, customer.state
from sales
join customer
    on sales.cid = customer.id
join product
    on sales.pid = product.id
group by customer.gender, product.name, customer.state;
