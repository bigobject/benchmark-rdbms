CREATE DATABASE test;

USE test

CREATE TABLE `customer` (
  `id` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `language` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `age` int(11) DEFAULT NULL
);

LOAD DATA INFILE '/backup/customer.csv'
INTO TABLE customer
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\r\n';

CREATE TABLE `product` (
  `id` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `brand` varchar(255) DEFAULT NULL,
  `brandOwner` varchar(255) DEFAULT NULL,
  `weightGrams` float DEFAULT NULL,
  `weightOunce` float DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `cost` float DEFAULT NULL,
  `profit` float DEFAULT NULL
);

LOAD DATA INFILE '/backup/product.csv'
INTO TABLE product
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\r\n';

CREATE TABLE `sales` (
  `order_id` varchar(255) DEFAULT NULL,
  `cid` varchar(255) DEFAULT NULL,
  `pid` varchar(255) DEFAULT NULL,
  `channel_name` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `total_price` float DEFAULT NULL
);

LOAD DATA INFILE '/backup/sales.csv'
INTO TABLE sales
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\r\n';
