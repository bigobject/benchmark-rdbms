This is what this project does
------------------------------

There are two preconfigured docker scripts that starts offical images of
postgres and mysql database.

- To begin testing, launch the appropriate docker image and connect using mysql
  client of postgres client.  Follow the instructions on docker hub postgres or
  mysql to connect without installing client.

- Initiate the database with postgres.sql or mysql.sql script.  The assumption
  is your container mounted this directory to */backup* in the container.

- Test statements from example.sql for benchmark results.
